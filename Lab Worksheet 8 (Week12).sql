USE master;
IF EXISTS(SELECT * FROM sys.databases WHERE name='Students') DROP DATABASE Students;
CREATE database Students;
GO
USE Students;
CREATE TABLE Student (RollNumber int primary key, StudentName varchar(25), RegisterDate date, Gender bit, DegreeName varchar(25), CGPA float);
CREATE TABLE Preference (RollNumber int, Preference varchar(25));
BULK INSERT Student FROM 'path/to/csv' WITH (FIELDTERMINATOR = ',', ROWTERMINATOR = '0x0a', FIRSTROW = 2)
BULK INSERT Preference FROM 'path/to/csv' WITH (FIELDTERMINATOR = ',', ROWTERMINATOR = '0x0a', FIRSTROW = 2)

